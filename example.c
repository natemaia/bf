#define BF_IMPLIMENTATION
#include "bf.h"


int main(int argc, char *argv[])
{
	bool *h = bf_bool("h", false, "Print this help message and exit", false);
	int *i = bf_int("i", 3840, "This argument holds an integer", true);
	float *f = bf_float("f", 0.0, "This argument holds a float", true);
	char *s = bf_str("s", "", "This argument holds a string", true);

	if (bf_parse(argc, argv) < 0)
		return 1;

	if (*h)
		bf_usage(argv[0], stdout);

	if (bf_state.argc)
		for (int j = 0; j < bf_state.argc; j++)
			printf("bf_state.argv[%d] = %s\n", j, bf_state.argv[j]);

	printf("\nh: %d\ni: %d\nf: %f\ns: %s\n", *h, *i, *f, s);

	return 0;	
}
