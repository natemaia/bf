/*
 * Basic Flag handling library for command line arguments
 *
 * Written by: Nathaniel Maia <natemaia10@gmail.com> 2021-2022
 *
 * Create a flag entry of a certain type. Returns a pointer to where the value
 * will be stored after parsing. Once all flags have been defined we call bf_parse
 * to parse the command line arguments and populate the values. bf_usage will
 * print the flags and info in the standard way.
 *
 * bool *hflag = bf_bool("h", false, "Print this help and exit")
 *
 * bf_parse(argc, argv);
 *
 * if (*hflag)
 *     bf_usage();
 *
 */

/*****************************************************/

#ifndef BF_H_INCLUDE
#define BF_H_INCLUDE

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

/* default maximum number of flags */
#ifndef BF_MAX_FLAGS
#define BF_MAX_FLAGS 32
#endif /* BF_MAX_FLAGS */


/* types of values accepted as arguments */
typedef enum BF_Type {
	BF_integer,
	BF_string,
	BF_floating,
	BF_boolean
} BF_Type;

/* error types of values accepted as arguments */
typedef enum BF_Error {
	BF_ENONE,
	BF_NOVAL,
	BF_BADVAL,
	BF_NOTFLAG
} BF_Error;

/* storage for one (and only one) of the types accepted as arguments */
typedef union Arg {
	int integer;
	char *string;
	float floating;
	bool boolean;
} Arg;

/* storage for each flag with everything about it */
typedef struct BF_Flag {
	BF_Type type;
	bool printdef;
	const char *flag, *desc;
	Arg val, def;
} BF_Flag;

/* storage for state of flag parsing */
typedef struct BF_State {
	BF_Error error;
	int argc;
	char e_str[1024], *e_flag, *argv[];
} BF_State;


/* Args:
 *		flag     - a string constant to use as the flag (without -)
 *		def      - type specific default value to use as the flag value
 *		desc     - a string constant to use when printing usage info
 *		printdef - determines whether the default value should be printed with usage info
 */
int  *bf_int(const char *flag, int def, const char *desc, bool printdef);
char *bf_str(const char *flag, char *def, const char *desc, bool printdef);
float *bf_float(const char *flag, float def, const char *desc, bool printdef);
bool *bf_bool(const char *flag, bool def, const char *desc, bool printdef);

int bf_parse(int argc, char *argv[]);
void bf_usage(char *argv0, FILE *stream);


#endif /* BF_H_INCLUDE */



/*****************************************************/


#ifdef BF_IMPLIMENTATION

static int      bf_used_flags = 0;
static BF_Flag  bf_flags[BF_MAX_FLAGS];
static BF_State bf_state = { BF_ENONE, 0, { '\0' }, NULL };

#define BF_NEW(ftype, val, ddef, flag, def, desc, pdef) \
	if (bf_used_flags >= BF_MAX_FLAGS) return NULL;     \
	bf_used_flags++;                                    \
	f->type = ftype;                                    \
	f->printdef = pdef;                                 \
	f->flag = flag;                                     \
	f->desc = desc;                                     \
	ddef = def;                                         \
	val = def

int *bf_int(const char *flag, int def, const char *desc, bool printdef)
{
	BF_Flag *f = &bf_flags[bf_used_flags];
	BF_NEW(BF_integer, f->val.integer, f->def.integer, flag, def, desc, printdef);
	return &(f->val.integer);
}

char *bf_str(const char *flag, char *def, const char *desc, bool printdef)
{
	BF_Flag *f = &bf_flags[bf_used_flags];
	BF_NEW(BF_string, f->val.string, f->def.string, flag, def, desc, printdef);
	return f->val.string;
}

float *bf_float(const char *flag, float def, const char *desc, bool printdef)
{
	BF_Flag *f = &bf_flags[bf_used_flags];
	BF_NEW(BF_floating, f->val.floating, f->def.floating, flag, def, desc, printdef);
	return &(f->val.floating);
}

bool *bf_bool(const char *flag, bool def, const char *desc, bool printdef)
{
	BF_Flag *f = &bf_flags[bf_used_flags];
	BF_NEW(BF_boolean, f->val.boolean, f->def.boolean, flag, def, desc, printdef);
	return &(f->val.boolean);
}
#undef BF_NEW


static int _bf_strcmp(const char* s1, const char* s2)
{
    while (*s1 && (*s1 == *s2)) s1++, s2++;
    return *(const unsigned char*)s1 - *(const unsigned char*)s2;
}

static int _bf_errfill(char *arg, BF_Error e, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	vsnprintf(bf_state.e_str, sizeof(bf_state.e_str), fmt, args); // NOLINT
	va_end(args);
	bf_state.error = e;
	bf_state.e_flag = arg;
	return -1;
}

int bf_parse(int argc, char *argv[])
{
	for (int i = 1; i < argc && argv[i]; i++) {
		char *end, *val = NULL, *arg = argv[i];

		/* non-flag argument, push it back into bf_state.argv */
		if (*arg != '-') {
			bf_state.argv[bf_state.argc++] = argv[i];
			continue;
		}
		arg++; /* skip over the leading dash */
		
		/* find an equal sign '=', split the string and store the value following the '=' */
		for (char *s = arg; *s; s++) {
			if (*s == '=')	{
				*s = '\0';
				val = ++s;
				fprintf(stderr, "found -flag=arg, flag: %s, arg: %s\n", arg, val);
				break;
			}
		}

		/* match a flag and handle the argument (if any) */
		int match = 0;
		for (int j = 0; j < bf_used_flags; j++) {
			if (!_bf_strcmp(bf_flags[j].flag, arg)) {
				long l;
				float f;
				BF_Flag *flag = &bf_flags[j];
				val = val ? val : argv[i + 1];
				match = 1;

				switch (flag->type) {
				case BF_integer:
					if (!val || *val == '\0')
						return _bf_errfill(arg, BF_NOVAL, "error: expected argument for -%s INTEGER\n", arg);
					if (!(((l = strtol(val, &end, 0)) || !_bf_strcmp("0", val)) && *end == '\0'))
						return _bf_errfill(arg, BF_BADVAL, "error: invalid argument for -%s: %s - expected INTEGER\n", arg, val);
					flag->val.integer = l;
					i++;
					break;
				case BF_string:
					if (!val || *val == '\0')
						return _bf_errfill(arg, BF_NOVAL, "error: invalid argument for -%s: %s - expected STRING\n", arg, val);
					flag->val.string = val;
					i++;
					break;
				case BF_floating:
					if (!val || *val == '\0')
						return _bf_errfill(arg, BF_NOVAL, "error: expected argument for -%s FLOAT\n", arg);
					if (!(((f = strtof(val, &end)) || !_bf_strcmp("0.0", val)) && *end == '\0'))
						return _bf_errfill(arg, BF_BADVAL, "error: invalid argument for -%s: %s - expected FLOAT\n", arg, val);
					flag->val.floating = f;
					i++;
					break;
				case BF_boolean:
					if (val && *val != '\0')
						return _bf_errfill(arg, BF_BADVAL, "error: toggle flags take no arguments: -%s: %s\n", arg, val);
					flag->val.boolean = true;
					break;
				}
				break;
			}
		}
		if (!match)
			return _bf_errfill(arg, BF_NOTFLAG, "error: unknown flag: -%s\n", arg);
	}
	return 1;
}

void bf_usage(char *argv0, FILE *stream)
{
	int first = 1;

	fprintf(stream, "\nusage: %s", argv0);

	for (int i = 0; i < bf_used_flags; i++) {
		if (bf_flags[i].type == BF_boolean) {
			if (first) { first = 0; fprintf(stream, " [\e[1m-"); }
			fprintf(stream, "%s", bf_flags[i].flag);
		}
	}
	if (!first) fprintf(stream, "\e[0m]");

	for (int i = 0; i < bf_used_flags; i++) {
		switch (bf_flags[i].type) {
		case BF_integer:  fprintf(stream, " [\e[1m-%s\e[0m \e[1mINTEGER\e[0m]", bf_flags[i].flag); break;
		case BF_string:   fprintf(stream, " [\e[1m-%s\e[0m \e[1mSTRING\e[0m]",  bf_flags[i].flag); break;
		case BF_floating: fprintf(stream, " [\e[1m-%s\e[0m \e[1mFLOAT\e[0m]",   bf_flags[i].flag); break;
		case BF_boolean:  break;
		}
	}
	fprintf(stream, "\n\n");

	for (int i = 0; i < bf_used_flags; i++) {
		if (!bf_flags[i].printdef) {
			fprintf(stream, "\t\e[1m-%s\e[0m\n\t    %s.\n\n", bf_flags[i].flag, bf_flags[i].desc);
			continue;
		}
		fprintf(stream, "\t\e[1m-%s\e[0m\n\t    %s.\n\t    Default: ", bf_flags[i].flag, bf_flags[i].desc);
		switch (bf_flags[i].type) {
		case BF_integer:  fprintf(stream, "%d\n\n",   bf_flags[i].def.integer);                    break;
		case BF_string:   fprintf(stream, "%s\n\n",   bf_flags[i].def.string);                     break;
		case BF_floating: fprintf(stream, "%.2f\n\n", bf_flags[i].def.floating);                   break;
		case BF_boolean:  fprintf(stream, "%s\n\n",   bf_flags[i].def.boolean ? "true" : "false"); break;
		}
	}
}

#endif /* BF_IMPLIMENTATION */
// vim:ft=c
