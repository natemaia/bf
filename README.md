## bf.h - better flags
A very basic command line flag parsing and handling header-only library
with no dynamic memory allocation, no cleanup, a simple API, and a low memory footprint.

It currently handles four data types for flags: integers, floats, strings, and booleans.
Boolean flags don't require an argument, for basic flags like `-h,-help` and `-v,-version`.

No support for long format arguments and only one dash is expected/stripped during parsing.

A basic example C file is provided, to compile it use
```bash
cc example.c -o example
```

Then run it with various flags
```bash
./example -h
```


### Usage

To include the implementation side of the header first define `BF_IMPLEMENTATION`
```
#define BF_IMPLIMENTATION
#include "bf.h"
```

There are a number of functions to create a flag entry of a certain type.
These return a pointer to where the value will be stored after parsing.
Once all flags have been defined we call `bf_parse()` to parse the command
line arguments and populate the values. `bf_usage()` will print the flags and
info in the standard way.

```C
bool *hflag = bf_bool("h", false, "Print this help and exit", false);

bf_parse(argc, argv);

if (*hflag)
	bf_usage();
```

By default only space for 32 flags, to increase this number first define `BF_MAX_FLAGS`
then include the header after.
```
#define BF_MAX_FLAGS 64
```


### API

The header defines six externally available functions, four are to define a new flag:
```
int *bf_int(const char *flag, int def, const char *desc, bool printdef);
char *bf_str(const char *flag, char *def, const char *desc, bool printdef);
float *bf_float(const char *flag, float def, const char *desc, bool printdef);
bool *bf_bool(const char *flag, bool def, const char *desc, bool printdef);
```
Arguments for the flag functions are as follows:
- `flag` a string constant to use as the flag (without -)
- `def` type specific default value to use as the flag value
- `desc` a string constant to use when printing usage info
- `printdef` determines whether the default value should be printed with usage info

The other two parse the flags and print usage text to a stream respectively
```
int bf_parse(int argc, char *argv[]);
void bf_usage(char *argv0, FILE *stream);
```
- `argc` and `argv` passed from `main()`
- `argv0` the name of the program or `argv[0]` in main.
- `stream` where to output usage info, eg. `stderr`


Arguments that didn't match any flags will be counted and stored in
```
bf_state.argc  // int       - how many arguments were pushed back
bf_state.argv  // char *[]  - array of the arguments
```


Upon error `bf_parse()` will return `-1` and information will be placed in `bf_state`
```
bf_state.error  // enum BF_Error - what type of error occurred
bf_state.e_flag // char *        - flag that caused the error
bf_state.e_str  // char []       - printable description of the error
```
